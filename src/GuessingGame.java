import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Game business logic
 */
public class GuessingGame {

    /**
     * RNG instance
     */
    private static final Random RANDOM_NUMBER_GENERATOR = new Random();

    /**
     * Our secret number will be generated once per instance. Making it final ensures we'll not be messing with it.
     */
    private final int secretNumber;
    /**
     * Input scanner instance
     */
    private final Scanner inputScanner;
    /**
     * Maximum amount of attempts
     */
    private final int maxAttempts;
    /**
     * List of choices made
     */
    private Set<Integer> guesses = new HashSet<>();

    /**
     * Convenience constructor calling full constructor with defaulted values
     *
     * @param inputScanner Input for player choice
     */
    public GuessingGame(Scanner inputScanner) {
        this(inputScanner, 5);
    }

    /**
     * Full constructor with all parameters
     * Generates a random number as our {@link #secretNumber}.
     *
     * @param inputScanner Input for player choice
     * @param maxAttempts  Maximum attempts allowed
     */
    public GuessingGame(Scanner inputScanner, int maxAttempts) {
        this.secretNumber = RANDOM_NUMBER_GENERATOR.nextInt(16);
        this.inputScanner = inputScanner;
        this.maxAttempts = maxAttempts;
    }

    /**
     * Game loop
     */
    public void playGuessingGame() {
        System.out.println("Ok, I just thought of a new number for you to guess.");

        while (guesses.size() < maxAttempts) {
            if (queryGuess()) return;
        }

        System.out.println("You ran out of attempts.");
    }

    /**
     * Game tick
     *
     * @return Tick win condition. true if guess was correct.
     */
    private boolean queryGuess() {
        // I was to lazy to write it in a more understandable way... Streaming API is way to convenient.
        // It just joins all elements of the set with a ", "
        System.out.printf("So far you've guessed: %s%n", guesses.stream().map(String::valueOf).collect(Collectors.joining(", ")));
        System.out.printf("You have %d guesses left%n", maxAttempts - guesses.size());
        System.out.println("Your next guess?");

        // Get our new input
        int guess = inputScanner.nextInt();

        // If the number was already guessed, don't store the number, report the issue and return a failed state.
        if (guesses.contains(guess)) {
            System.out.printf("You've already guessed %d.%n", guess);
            return false;
        }

        // If we guess the number, let the user know, then return a success state.
        if (guess == secretNumber) {
            System.out.println("Great guess!!! That's my number!! You _SOO_ win!!");
            return true;
        }

        // Otherwise add our guess to the guess set and give the user a hint, before returning a failed state.
        guesses.add(guess);
        System.out.printf("I'm sorry, but my secret number is not %d, my secret number is %s than %d%n", guess, guess < secretNumber ? "greater" : "lesser", guess);
        return false;
    }
}