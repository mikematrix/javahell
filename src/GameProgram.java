import java.util.Scanner;

/**
 * Executable Entry Class
 */
public class GameProgram {

    /**
     * Scanner for input handling.
     * <p>
     * Defining this here, then passing it along allows us to replace the implementation of it, and replace all usages.
     */
    private static final Scanner INPUT_SCANNER = new Scanner(System.in);

    /**
     * Main Method
     *
     * @param args No arguments
     */
    public static void main(String[] args) {
        // Generate a new instance for each round.
        // We're in essence restarting the game.
        // Do-While, so it will get executed, BEFORE we make a check if we want to restart the game.
        do {
            // Instantiate the game, pass along our input scanner instance and execute the play method.
            new GuessingGame(INPUT_SCANNER).playGuessingGame();
        } while (checkContinue()); // Query the user for repeating the game.
    }

    /**
     * Query the user if they want to continue playing a new round.
     * Will output text instructions to {@link System#out}
     *
     * @return true if continue is selected
     */
    private static boolean checkContinue() {
        System.out.print("Do you want to continue? (y/n): ");
        String input = INPUT_SCANNER.next().toLowerCase();
        switch (input) {
            case "1":
            case "y":
            case "yes":
                return true;
            case "0":
            case "n":
            case "no":
                System.out.println("Have a nice day!");
                return false;
            default:
                System.out.println("Invalid input.");
                return checkContinue();
        }
    }
}